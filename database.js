const Mongoose = require('mongoose');
const config = require('./config.js');
Mongoose.connect('mongodb://' + config.database.host +':'+ config.database.port + '/' + config.database.db);
const db = Mongoose.connection;
db.on('error', console.error.bind(console, 'connection with databse error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});
exports.Mongoose = Mongoose;
exports.db = db;