const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var TodoSchema = new Schema({
    userId : { type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    todoName : { type: String, required: true },
    todoDate :  { type: String, required: true },
    todoStatus : { type: String, required: true}
});

var todo = mongoose.model('todos', TodoSchema);

module.exports=todo;