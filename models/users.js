'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    userName : { type: String, required: true, unique: true },
    userPassword : { type: String, required: true }
});

var user = mongoose.model('users', UserSchema);

module.exports=user;