const Todo = require('../models/todos.js');
const Joi = require('joi');


function routes (server)
{

// Route works with /todos 
// REST API work only for user who send jwt (not for all users for better security)
// For work with all users data need to create another route

server.route({
    method: 'GET',
    path: '/todos',
    config: { auth: 'jwt' },
    handler: async (request, h) => {
        try {
            const todos = await Todo.find({'userId':request.auth.credentials.userId});
            const response = h.response(todos)
                .code(200) 
                .header('Content-Type', 'application/json');
            return response;
        } catch (err) {
            const response = h.response({message: 'todo jobs not found'})
            .code(200) 
            .header('Content-Type', 'application/json');
            return response;   
        }
      }
});

server.route({
    method: 'GET',
    path: '/todos/{id}',
    config: { auth: 'jwt' },
    handler: async (request, h) => {
        try {
            const todos = await Todo.findOne({_id: (request.params.id)});
            const response = h.response(todos)
                .code(200) 
                .header('Content-Type', 'application/json');
            return response;
        } catch (err) {
            const response = h.response(`todo job with id=${request.params.id} not found cause ${err}`)
            .code(200) 
            .header('Content-Type',  'text/html; charset=utf-8');
            return response;   
        }
      }
});

server.route({
    method: 'POST',
    path: '/todos',
    config: {
        auth: 'jwt',
        validate: {
            payload: {
                todoName: Joi.string().required(),
                todoDate: Joi.string().required(),
                todoStatus: Joi.string().required()
            }    

        }
    },
    handler: async (request, h)=>{
        try {
                const newtodo = new Todo({userId: request.auth.credentials.userId, todoName: request.payload.todoName,
                    todoDate: request.payload.todoDate, todoStatus: request.payload.todoStatus});
                await newtodo.save();
                const response = h.response(`'Todo job succesfully created.`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                const response = h.response(`Can't create new todo job ${err}`)
                .code(403) 
                .header('Content-Type', 'application/json');
                return response;
            }    
        }
       
});  

server.route({
    method: 'DELETE',
    path: '/todos',
    config: { auth: 'jwt' },
    handler: async (request, h)=>{
        try {
                const todos = await Todo.remove({'userId':request.auth.credentials.userId});
                //await todos.remove();
                const response = h.response(`All jobs of user with id=${request.auth.credentials.userId}successfully deleted`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                const response = h.response(`Can't delete user jobs, cause ${err}`)
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
}); 

server.route({
    method: 'DELETE',
    path: '/todos/{id}',
    config: { auth: 'jwt' },
    handler: async (request, h)=>{
        try {
            const todofordel=await Todo.findByIdAndRemove(request.params.id);
            const response = h.response(`Job with id=${request.params.id} successfully deleted`)
            .code(201) 
            .header('Content-Type', 'text/html; charset=utf-8');
            return response;
        }
    catch (err)
        {
            console.log (err);
            const response = h.response(`Can't delete job with id=${request.params.id} cause ${err} `)
            .code(403) 
            .header('Content-Type', 'text/html; charset=utf-8');
            return response;
        }    
    }
       
}); 


server.route({
    method: 'PATCH',
    path: '/todos/{id}',
    config: { auth: 'jwt' },
    handler: async (request, h)=>{
        try {
                const todos = await Todo.findOneAndUpdate({_id: request.params.id},{$set:request.payload});
                const response = h.response(`Job with id=${request.params.id} successfully patched`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                console.log (err);
                const response = h.response({message:`Can't patch user with id=${userforpatch._id}`})
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
});   

}
module.exports = routes;