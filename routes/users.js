const User = require('../models/users.js');
const Joi = require('joi');
const mongoose = require('mongoose');
const Bcrypt = require('bcrypt');
const config = require('../config.js');
const JWT   = require('jsonwebtoken');

function routes (server)
{
// Route for works with /users
server.route({
    method: 'GET',
    path: '/auth',
    config: { 'auth': false },
    handler: async (request, h)=>{
        try {
             const users= await User.findOne({userName: request.query.userName});
             const isValid = await Bcrypt.compare(request.query.userPassword,users.userPassword);
             if (isValid)
             {
                const tokenData={"userName":request.query.userName, "userId":users._id};
                const jwt=JWT.sign(tokenData, config.security.privateKey, { expiresIn: config.security.tokenExpiry });
                console.log (jwt);
                const response = h.response(`Hi user ${request.query.userName} you got jwt=${jwt}`)
                .code(200) 
                .header ('Authorization', 'Bearer '+jwt)
                .header('Content-Type', 'text/html; charset=utf-8');   
                return response;
             }
             else
             {
               const response = h.response(`Encorrect password for user ${request.query.userName}.Please <a href=${server.info.uri}>login</a>`)
              .code(401) 
              .header('Content-Type', 'text/html; charset=utf-8');
              return response;
             }
    
            }
        catch (err)
            {
              const response = h.response(`No user with name ${request.query.userName}. Please <a href=${server.info.uri}>login</a>`)
              .code(401) 
              .header('Content-Type', 'text/html; charset=utf-8');
              return response;
            }    
        }
       
});  


server.route({
    method: 'GET',
    path: '/users/{id}',
    config: { 'auth': false },
    handler: async (request, h)=>{
        try {
                const users = await User.findOne({_id: encodeURIComponent(request.params.id)});
                const response = h.response(users)
                .code(200) 
                .header('Content-Type', 'application/json');
                return response;
            }
        catch (err)
            {
                const response = h.response(`user with _id=${request.params.id} not found cause ${err}`)
                .code(404) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
});   

server.route({
    method: 'DELETE',
    path: '/users',
    config: { 'auth': false },
    handler: async (request, h)=>{
        try {
                const users = await mongoose.connection.db.dropCollection('users');
                const response = h.response({message:`All users successfully deleted`})
                .code(201) 
                .header('Content-Type', 'application/json');
                return response;
            }
        catch (err)
            {
                const response = h.response(`Can't delete all users cause ${err}`)
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
}); 

server.route({
    method: 'DELETE',
    path: '/users/{id}',
    config: { 'auth': false },
    handler: async (request, h)=>{
        try {
                const userfordel=await User.findOne({_id: request.params.id});
                console.log('fordelete=',userfordel);
                const users = await User.deleteOne({_id: userfordel._id});
                const response = h.response(`User with id=${userfordel._id} successfully deleted`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                console.log (err);
                const response = h.response(`Can't delete user with id=${request.params.id} `)
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
}); 

server.route({
    method: 'POST',
    path: '/users',
    config: { 
        auth: false,
        validate: {
            payload: {
                userName: Joi.string().required(),
                userPassword : Joi.string().required()
            }    

        }
    },
    handler: async (request, h)=>{
        try {

                const hashed= await Bcrypt.hash(request.payload.userPassword, config.security.saltRounds);
                console.log ('post user with hashed password=', hashed);
                const newuser = new User({userName: request.payload.userName, userPassword: hashed});
                await newuser.save();
                const response = h.response(`'User ${request.payload.userName} succesfully created.Please <a href="/">login</a>`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                console.log (err);
                const response = h.response(`Can't create new user cause ${err}`)
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
});  

server.route({
    method: 'PATCH',
    path: '/users/{id}',
    config: { 'auth': false },
    handler: async (request, h)=>{
        try {
                const userforpatch=await User.findOne({_id: request.params.id});
                const users = await User.findOneAndUpdate({_id: userforpatch._id},{$set:request.payload});
                const response = h.response(`User with id=${userforpatch._id} successfully patched`)
                .code(201) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }
        catch (err)
            {
                console.log (err);
                const response = h.response(`Can't patch user with id=${userforpatch._id} cause ${err}`)
                .code(403) 
                .header('Content-Type', 'text/html; charset=utf-8');
                return response;
            }    
        }
       
});   

server.route({
    method: 'GET',
    path: '/users',
    config: { 'auth': false },
    handler: async (request, h) => {
        try {
            const users = await User.find({});
            const response = h.response(users)
               .code(200) 
               .header('Content-Type', 'application/json');
            return response;
        } catch (err) {
            const response = h.response(`Cant get all users, cause ${err}`)
            .code(403) 
            .header('Content-Type', 'application/json');
            return response;   
        }
      }
});    

}
module.exports = routes;