function registerRoutes(server)
{
// Here we register all our routes in ./routes by hands
// It would be better to get all files in ./routes
// and register it automatically - only add a file in ./routes and voilya
const static=require('./static.js');    
static(server);
const users=require('./users.js');    
users(server);
const todos=require('./todos.js');    
todos(server);
return;
}
module.exports = registerRoutes;