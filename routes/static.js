function routes (server)
{
// Route for static pages
server.route({
    method: 'GET',
    path: '/',
    config: { 'auth': false },
    handler: (request, h) => {
        return h.file('./public/index.html');
    }
});
server.route({
    method: 'GET',
    path: '/new',
    config: { 'auth': false },
    handler: (request, h) => {
        return h.file('./public/newuser.html');
    }
});

}
module.exports = routes;