module.exports = {
    server: {
        host: '0.0.0.0',
        port: 8000
    },
    database: {
        host: '127.0.0.1',
        port: 27017,
        db: 'todolist',
        username: '',
        password: ''
    },
    security: {
        privateKey: 'HrenVamVsemANePrivatKey',
        tokenExpiry: 1 * 60 * 60,
        saltRounds: 12 
    }
};