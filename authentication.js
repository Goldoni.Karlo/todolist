const AuthJWT = require('hapi-auth-jwt2');
const User = require('./models/users.js');
const config = require('./config.js');

function registerAuthentication (server)
{
// Register authentication strategy
const aus_jwt_validate=async function (decoded, request) {
        console.log ('Validator');
        console.log (decoded.userName, decoded.iat, decoded.exp, decoded.userId);
        try {
           const users = await User.findOne({userName: decoded.userName});
           console.log (users);
           const now=new Date();
           const nowtime=parseInt(now.getTime()/1000); 
           if (decoded.exp<nowtime) 
              {
                console.log (`${nowtime}>${decoded.exp} token expired`);
                return {isValid: false, message:'Token expired'}
              }
           else  {
              console.log (`${nowtime}<${decoded.exp} token not expired`);
              request.query.userName=decoded.userName;
              return {isValid: true, message:'It is OK'}}   
            }
        catch (err)
            {
            return {isValid: false, message:'User not found'}    
            }    

};

const init = async () => {
  try {
      await server.register(AuthJWT);
      server.auth.strategy('jwt', 'jwt',
      { key: config.security.privateKey,         
        validate: aus_jwt_validate
        //,verifyOptions: { algorithms: [ 'HS256' ] } 
      });
      server.auth.default('jwt');
      console.log ('jwt registered');
      }
  catch (err)
      {
        console.log('AuthError=');
      }

};
init();
};
module.exports = registerAuthentication;