const hapi=require('hapi');
const config = require('./config.js');
const routes=require('./routes/index.js');
const db = require('./database.js');
const auth = require('./authentication.js')

// Create a server with a host and port
const server=new hapi.server(config.server);

// Start the server
const init = async () => {
    await auth(server);
    await server.register(require('inert'));
    await server.start();
    routes(server);
    console.log(`Server running at: ${server.info.uri}`);
};

init();